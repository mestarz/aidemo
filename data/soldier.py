from data.pointer import Pointer


class Soldier(object):

    def __init__(self):
        self.life = 100  # 生命值
        self.atk = 20  # 攻击力
        self.step = 3  # 最大步长
        self.id = 0  # 初始id
        self.location = Pointer(0, 0)

    def set_location(self, x, y):
        self.location.set(x, y)
